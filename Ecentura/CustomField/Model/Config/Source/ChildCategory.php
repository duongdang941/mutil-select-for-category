<?php

namespace Ecentura\CustomField\Model\Config\Source;

use Magento\Catalog\Model\CategoryRepository;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template\Context;
use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
class ChildCategory extends AbstractSource
{
    protected $registry;

    protected $categoryRepository;

    public function __construct(
        Registry           $registry,
        CategoryRepository $categoryRepository,
    ) {
        $this->registry = $registry;
        $this->categoryRepository = $categoryRepository;
    }

    public function toOptionArray()
    {
        $result = [];
        $currentCategory = $this->getCurrentCategory();
        $childCategories = $currentCategory->getChildrenCategories()->getItems();

        if (!empty($childCategories)) {
            foreach ($childCategories as $childCategory) {
                $result[] = [
                    'value' => $childCategory->getId(),
                    'label' => $childCategory->getName(),
                ];
            }
        }

        return $result;
    }

    public function getCurrentCategory()
    {
        return $this->registry->registry('current_category');
    }

    public function getAllOptions()
    {
        $res = $this->getOptions();
        array_unshift($res, ['value' => '', 'label' => '']);
        return $res;
    }

}
