<?php

namespace Ecentura\CustomField\Setup\Patch\Data;

use Magento\Catalog\Model\Category;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

class CategoryAttribute implements DataPatchInterface {

    public $moduleDataSetup;

    public $eavSetupFactory;
    /**
     * Dependency Initilization
     *
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
    }

    public function apply()
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);
        /* Remove Category Attribute */
        $eavSetup->removeAttribute(Category::ENTITY, 'child_categories_hidden');

        /* Add Category Attribute */
        $eavSetup->addAttribute(
            Category::ENTITY,
            'child_categories_hidden',
            [
                'type' => 'text',
                'label' => 'Child Categories Hidden',
                'input' => 'multiselect',
                'visible' => true,
                'required' => false,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
                'group' => 'General Information',
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function getAliases()
    {
        return [];
    }


}
