<?php

namespace Ecentura\CustomField\Plugin;

use Magento\Framework\Serialize\SerializerInterface;
use Magento\Catalog\Controller\Adminhtml\Category\Save;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Psr\Log\LoggerInterface as Logger;

class AfterSaveCategoryPlugin
{
    protected $serializer;

    public function __construct(
        CategoryRepositoryInterface $categoryRepository,
        SerializerInterface         $serializer,
        Logger                      $logger
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->serializer = $serializer;
        $this->logger = $logger;
    }

    public function afterExecute(Save $subject, $result)
    {
        try {
            $categoryPostData = $subject->getRequest()->getPostValue();
            if($categoryPostData) {
                $category = $this->categoryRepository->get($categoryPostData['entity_id'], $categoryPostData['store_id']);
                if ($category) {
                    $childCategoriesHidden = isset($categoryPostData['child_categories_hidden']) ? $categoryPostData['child_categories_hidden'] : '';
                    $data = $this->serializer->serialize($childCategoriesHidden);
                    $category->setData('child_categories_hidden', $data);
                    $this->categoryRepository->save($category);
                }
            }

            return $result;
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            return $result;
        }
    }
}
