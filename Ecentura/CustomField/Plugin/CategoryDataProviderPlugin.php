<?php

namespace Ecentura\CustomField\Plugin;

use Magento\Catalog\Model\Category\DataProvider;
use Magento\Framework\Serialize\SerializerInterface;
use Psr\Log\LoggerInterface as Logger;

class CategoryDataProviderPlugin
{
    /**
     * @var Logger
     */
    protected $logger;

    public $serializer;
    public function __construct(
        SerializerInterface $serializer,
        Logger $logger
    ) {
        $this->serializer = $serializer;
        $this->logger = $logger;
    }

    public function afterGetData(DataProvider $subject, $result)
    {
        try {
            $category = $subject->getCurrentCategory();
            if ($category && isset($result[$category->getId()]['child_categories_hidden'])) {
                $childCategoryHidden = $result[$category->getId()]['child_categories_hidden'];
                $result[$category->getId()]['child_categories_hidden'] = $this->serializer->unserialize($childCategoryHidden);
            }
            return $result;

        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            return $result;

        }
    }
}
